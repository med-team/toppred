toppred (1.10-10) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Sat, 26 Nov 2022 20:46:50 +0100

toppred (1.10-9) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 06 Oct 2021 16:47:09 +0200

toppred (1.10-8) unstable; urgency=medium

  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * watch file standard 4 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 02 Dec 2020 21:17:36 +0100

toppred (1.10-7) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Explicitly pass paths to configure instead of looking up tools
    - fixes reproducible builds on merged-usr vs non-merged systems
    Closes: #915853

  [ Andreas Tille ]
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Sat, 08 Dec 2018 07:37:13 +0100

toppred (1.10-6) unstable; urgency=medium

  * d/rules: do not parse d/changelog
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.5

 -- Andreas Tille <tille@debian.org>  Mon, 16 Jul 2018 11:36:54 +0200

toppred (1.10-5) UNRELEASED; urgency=medium

  * debian/upstream/metadata:
    - Added references to registries
    - yamllint cleanliness

 -- Steffen Moeller <moeller@debian.org>  Thu, 14 Sep 2017 10:25:49 +0200

toppred (1.10-4) unstable; urgency=medium

  * Team upload.
  * Make build reproducible.
    - Remove entries with build directories (which would be gone anyway)
      from generated Makefiles in examples directory.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 27 Aug 2016 09:51:57 +0000

toppred (1.10-3) unstable; urgency=medium

  * Team upload.
  * Make build reproducible.
    - Fix $SHELL/CONFIG_SHELL to /bin/sh.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Wed, 25 May 2016 21:15:14 +0000

toppred (1.10-2) unstable; urgency=medium

  * Fix autopkgtest
  * cme fix dpkg-control
  * Better hardening

 -- Andreas Tille <tille@debian.org>  Fri, 29 Apr 2016 16:15:34 +0200

toppred (1.10-1) unstable; urgency=low

  * Initial release (Closes: #776709)

 -- Andreas Tille <tille@debian.org>  Sat, 31 Jan 2015 12:59:46 +0100
